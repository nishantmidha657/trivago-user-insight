
Steps to run:-


1)Run buildImage.sh script from same directory to build the image which result into image with name trivago-user-insight.

2) Run the image using below command,  :-

NOTE:- 
1) Please replace $localM2 with path to your local .m2 and replace $ipaddress with system ipaddress
2) Run command mkdir -p trivago-logs in your host machine to mount it for logs

Mounting local .m2 in case of any issues in downloading jars inside docker container and avoiding downloading again and again.

docker run -d --name trivago-userinsight -v $localM2:/root/.m2 -v trivago-logs:trivago/trivago-logs -p $ipaddress:8081:8080 trivago-user-insight


for example:-

docker run -d --name trivago-userinsight -v /home/nishant/.m2:/root/.m2 -p 10.152.151.180:8081:8080 trivago-user-insight


once the docker container is up. You can use below URLS for both insights:-

For first insight:


http://ipaddress:8081anemities/user/$userId/$count


For Second insight:-

http://ipaddress:8080/hotels/user/$userId/$count

Replace $userId and $count with values

1) What are the assumptions that you made during the implementation?

-> Data provided in excel sheet is in descending order of timestamp for each userId.
-> CSV file can be changed so not loading complete csv in memory
-> There might be some issue in source csv as I could see userId value was very long and reading userId value from csv gives different value in java  eg the last digit:-- 2835680135910990 was converted to 2835680135910998. I am not really sure about formating of data in csv. So created the application using some userId as read n java. 
->Hotel id can be same for different region for same hotel. so Considered combination of hotelid and region to calculate the clicks count.

For first user animities insight, I considered returning only first top N entries found in selections.csv
For ex:-
http://127.0.0.1:8080/anemities/user/$userId/$count

replace $userId with any valid id like  2835680135910998
replace $count with number oif entries required

For second user insight of getting max hotel clicks , considered reading complete csv files and returning results based on max clicks.

http://ipaddress:8080/hotels/user/$userId/$count

replace $userId with any valid id like 414007124669993000
replace $count with number oif entries required


-> Code is definitly not validation proof. You can expect validation missing.

2) What are the performance characteristics of your implementation?

For first user animities insight, I considered returning only first top N entries found in selections.csv. so as soon as number of entries required are found. Result is returned 

For ex:-
http://127.0.0.1:8080/anemities/user/2835680135910998/10

output would be animities id read from csv:-
["967149002","7505603","212820802","337409602"]


For Second User insight as Whole csv is required to be read, Created a separate threadPool for affloading the small processing part to threadpool. Only one thread(request thread) is reading the entry and giving it to threadpool for further processing which is maintaing the clicks count for hotelids.

Please note that sometime you will see different result in output because thread pool threads are still processing the records and your request thread returned after reading complete csv. This is also an optimization to look at.
ex:-

http://127.0.0.1:8080/hotels/user/414007124669993000/10

{"3484382-unknown":4,"3484382-Nevada":2,"62267-unknown":2,"953765-Nevada":2}



3) If you could load test it, what do you expect to see in the result?

I don't see this solution as fully implimented. If i do load test , i expect some problems when multiple users try to read the csv files and more io operation will happen. But if load is not that high i expect it to work fine.

4) If you had more time, how would you improve your solution?

I would add capability of caching the results and reread csv only after some configurable interval.

I would definitly relook the csv parsing strategy based on information like below:-
1)how frequently csv changes
2)How frequently this job will run to get the data 
3)how much is the load
