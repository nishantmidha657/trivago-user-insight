package trivago.casestudy.userinsight;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HotelThreadPoolExecutor {

	Logger logger = LoggerFactory.getLogger(HotelThreadPoolExecutor.class);

	private ThreadPoolExecutor threadPoolExecutor;

	@Autowired
	public UserDataHolder userDataHolder;

	BlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>();

	@PostConstruct
	void initilize() {
		System.out.println("Thread pool Initialized");
		threadPoolExecutor = new ThreadPoolExecutor(15, 20, 5,
				TimeUnit.SECONDS, queue);
	}

	void submit(HotelDetailsWrapper hotelDetailsWrapper) {
		threadPoolExecutor.submit(hotelDetailsWrapper);
	}

	public int getQueueSize() {
		return queue.size();
	}

	public class HotelDetailsWrapper implements Runnable {

		private String[] csvRecord;
		private String userId;

		public HotelDetailsWrapper(String[] csvRecord, String userId) {
			this.csvRecord = csvRecord;
			this.userId = userId;
		}

		@Override
		public void run() {
			Map<String, Integer> hotelClicks;

			logger.debug("CSV Record: {},{},{},{}", csvRecord[0], csvRecord[1],
					csvRecord[2], csvRecord[3]);

			if (csvRecord[1].equalsIgnoreCase(userId)) {
				hotelClicks = userDataHolder.getHotelClicksCountForUser(userId);
				String key = csvRecord[2] + "-" + csvRecord[3];
				synchronized (hotelClicks) {
					if (hotelClicks.get(key) == null) {
						hotelClicks.put(key, 1);
					} else {
						hotelClicks.put(key, hotelClicks.get(key) + 1);
					}
				}

			}
		}
	}
}
