package trivago.casestudy.userinsight;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencsv.CSVReader;

@Service
public class UserInsightService {

	@Autowired
	public HotelThreadPoolExecutor threadPoolExecutor;

	@Autowired
	public UserDataHolder dataHolder;

	public Set<String> getUserAnimities(CSVReader csvReader, String userId,
			int animitiesCount) throws NumberFormatException, IOException {

		Set<String> csvRecords = new LinkedHashSet<String>();
		String[] csvRecord;
		while ((csvRecord = csvReader.readNext()) != null) {
			if (csvRecord[1].equalsIgnoreCase(userId)) {
				csvRecords.add(csvRecord[2]);
			} else {
				continue;
			}
			if (csvRecords.size() == animitiesCount) {
				return csvRecords;
			}
		}
		return csvRecords;
	}

	public Map<String, Integer> getMostClickedHotelsForUser(
			CSVReader csvReader, String userId, int hotelCount)
			throws IOException {

		String[] csvRecord;
		while ((csvRecord = csvReader.readNext()) != null) {
			threadPoolExecutor
					.submit(threadPoolExecutor.new HotelDetailsWrapper(
							csvRecord, userId));
			// System.out.println(Arrays.toString(csvRecord));
		}
		//waitForDataProcessing();
		//INPROVEMENT required, As data processing is offloaded to other threads which takes sometime and before that data is returned to user
		
		return dataHolder.removeUserId(userId);
	}

	private void waitForDataProcessing() {
		try {
			System.out
					.println("CSV File read completely,Wait for data processing..");
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
