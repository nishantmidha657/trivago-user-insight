package trivago.casestudy.userinsight;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.CSVParser;
import com.opencsv.CSVReader;

@RestController
public class UserInsightController {

	public Logger logger = LoggerFactory
			.getLogger(UserinsightApplication.class);

	@Value("${csv.read.path}")
	private String csvReadPath;

	@Value("${anemities.csv.file.name}")
	private String anemitiesFileName;

	@Value("${clicks.user.csv.file.name}")
	private String userClicksFile;

	@Autowired
	private UserInsightService userInsightService;

	@RequestMapping(value = "/anemities/user/{userid}/{count}")
	public ResponseEntity<Object> getUserAmenities(
			@PathVariable("userid") String id, @PathVariable("count") int count) {

		String filePath = csvReadPath + anemitiesFileName;
		Set<String> animities = null;
		try {
			Reader reader = Files.newBufferedReader(Paths.get(filePath));
			CSVReader csvReader = new CSVReader(reader);
			animities = userInsightService.getUserAnimities(csvReader, id,
					count);
		} catch (FileNotFoundException e) {
			logger.info("ALERT:File {} not found, Please check!!", filePath);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new ResponseEntity<>(animities, HttpStatus.OK);
	}

	@RequestMapping(value = "/hotels/user/{userid}/{count}")
	public ResponseEntity<Object> getMostClickedHotels(
			@PathVariable("userid") String id, @PathVariable("count") int count) {
		long time = System.currentTimeMillis();
		String filePath = csvReadPath + userClicksFile;
		Map<String, Integer> map = null;
		try {
			Reader reader = Files.newBufferedReader(Paths.get(filePath));
			CSVReader csvReader = new CSVReader(reader);
			map = userInsightService.getMostClickedHotelsForUser(csvReader, id,
					count);
			map = sortAndSendTopEntries(map, count);
		} catch (FileNotFoundException e) {
			logger.info("ALERT:File {} not found, Please check!!", filePath);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println((System.currentTimeMillis() - time) / 60);
		return new ResponseEntity<>(map, HttpStatus.OK);
	}

	public Map<String, Integer> sortAndSendTopEntries(Map<String, Integer> map,
			int count) {
		Map<String, Integer> map1 = new LinkedHashMap<String, Integer>();
		int temp = 0;

		map = map
				.entrySet()
				.stream()
				.sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
				.collect(
						Collectors.toMap(Map.Entry::getKey,
								Map.Entry::getValue, (e1, e2) -> e1,
								LinkedHashMap::new));

		if (map.entrySet().size() > count) {
			Iterator<Entry<String, Integer>> iterator = map.entrySet()
					.iterator();
			while (iterator.hasNext() && temp < count) {
				temp = temp + 1;
				Entry<String, Integer> entry = iterator.next();
				map1.put(entry.getKey(), entry.getValue());
			}
			map = map1;
		}

		return map;
	}
	
	@ExceptionHandler
	void handleException(Exception e){
		logger.warn("Exception occured, Please check {}",e);
	}
}
