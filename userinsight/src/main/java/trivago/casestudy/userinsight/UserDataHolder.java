package trivago.casestudy.userinsight;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

@Component
public class UserDataHolder {

	Map<String, Map<String, Integer>> hotelClickCount = new ConcurrentHashMap<String, Map<String, Integer>>();

	public Map<String, Integer> getHotelClicksCountForUser(String userId) {

		Map<String, Integer> map = hotelClickCount.get(userId);

		synchronized (this) {
			if (map == null) {
				addHotelClicksCountForUser(userId);
			}
		}
		return hotelClickCount.get(userId);
	}

	public synchronized void addHotelClicksCountForUser(String userId) {

		hotelClickCount.put(userId, new ConcurrentHashMap<String, Integer>());
	}

	public synchronized Map<String, Integer> removeUserId(String userId) {
		return hotelClickCount.remove(userId);
	}
}
